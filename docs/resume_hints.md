# Resume hints

![Placeholder](img/authors/francesco-calcavecchia.png){ align=left width="28" loading=lazy }

*Francesco Calcavecchia, 29 Jun 2022*

Here we collected several hints on how to write a good resume.

As hints were collected from several people and sources, don't expect them to be coherent.
They should serve the purpose of triggering reflections, and help you to make conscious decisions on how your
resume should look like.

## Resume or Curriculum Vitae?

|                        | Curriculum Vitae                        | Resume                                           |
|------------------------|-----------------------------------------|--------------------------------------------------|
| Content                | All your background                     | Background relevant for a specific job           |
| Typical for jobs       | In academia/research labs               | All others                                       |
| Length                 | Several pages                           | One page                                         |
| Typical reader         | Committee member (expert in your field) | HR employee (non-expert)                         |
| How it will be read    | Examined carefully                      | Examined for ~3-20 seconds                       |
| How it will be handled | Committee will compare all CVs          | All resumes are sorted into "yes" and "no" piles |

Our goal is to help you find a job outside academia / research labs, therefore **we will focus only on Resumes**.

Please keep in mind that Resumes and CVs are fundamentally different. Most people, especially in academia, are used
to CVs, and find difficult to write Resumes. Try not to fall into the mistake of writing a Resume as a shortened CV.

??? warning "What if the HR employee is used to CVs?"

    HR employees may also be used to CVs, and therefore get a bad impression from a Resume that, for example, 
    does not include all your background.

    So, how to behave? Unfortunately there is no universal recipe, and it is difficult to target a reader 
    you do not know and may vary a lot. Sometimes companies specify their Resume favorite format, 
    and you can refer to that. Otherwise you can try to compromise. For example: you would rather not mention a 
    2 years job experience as not functional for the job you are applying, however you fear that a "hole" 
    in your list of experiences may look bad. As a compromise, you could mention it while keeping it very brief, 
    for example mentioning only the role name, company, and time period.

    Also remember that, as long as HR will use only few seconds to judge your application, there is no real alternative 
    to a short and easy-to-consume Resume. Eventually they will be grateful for your efforts of making their job easier.

## Hints for a good Resume

### The undisputed hints

#### The do-s

Make sure to include: name, a contact like phone number and/or e-mail, home-page, 
link to GitHub/LinkedIn or similar professional platforms.

Address is usually expected but if you don't feel comfortable sharing it, then skip it 
([reference](https://www.indeed.com/career-advice/resumes-cover-letters/should-you-put-your-address-on-your-resume)).
Alternatively, as a compromise, you could just mention the city/region where you live.

#### The don't-do-s

Do not include your [date of birth or age](https://www.indeed.com/career-advice/resumes-cover-letters/date-of-birth-in-resume), 
marital status, number of children, or other personal information. 
It is illegal for employers to request this information of you.

#### The purpose is to get you an interview

When you write a Resume keep in mind that your goal is not to be exhaustive. The goal is to make them curious enough to
invite you for an interview.

#### Customization customization customization

The one-fit-all resume does not exist. You need to customize it for each job position. Even if you apply for the
same job role, different companies will require different skills, as it will emerge by reading the job description.
Make sure that all keywords appearing in the job description stand out in your Resume. Tip: you
could mark them in bold.

#### Be quantitative

Whenever possible be quantitative. Being quantitative demonstrate a fact-based mindset.

Rather than making a generic statement about your results, it is much more
impressive if you outline how it compares to others. Are you able to give a ground on how and why you stand out compared
to others? Did your publication have a big impact in your field? Can you prove
it by mentioning how many citations it received? And what is the average number of citations in your field? Did you
publish in a journal with a high impact factor? What is the percentage of publications in your field that achieve it?

It is relevant to be quantitative also for activities such as teaching. How many students did you have in the class? How
many hours of teaching?

#### Keep it simple

The primary goal of a Resume is to pass the HR filter. To achieve that, your Resume must sound convincing for people who
usually don't have technical expertises.

A simple exercise: give your Resume to your father or mother (hopefully they did not study in your same field). Ask them
what they understand and what they do not understand. It is totally fine if they do not understand the keywords
appearing in the job description, such as "C++", "parallel computing", or "stochastic processes". However, they should
get a rough idea of what you have done.

#### Implications of the 3-20 seconds rule

Another simple exercise: after few days without working at your Resume, try to open it and look at it as if you have
never seen it before, for 20 seconds only.
What are the first things that you notice? What are you able to take away in such very limited time?

This is the impression that you will give. Does this correspond to the message you would like to pass? If
not, how can you amend it? Perhaps you can work out the layout of the Resume to change this first impression?

#### How to present the publication list

Are your publications relevant for the job you are applying for? If not, perhaps you can just mention the number of 
publications and attach the list of publications as a separate document. If they are, then be sure to mention title, 
journal, and anything that can help to quantify its impact (e.g. number of citations, you are first author, ...).

#### Defeat stereotypes

It is common stereotype that researchers are loner, miss practicality, and are only interested in science and pure
research. You must break this barrier, and you can do so in several ways:

* mention the practical jobs you did: volunteering, teaching, contribution to open source projects, hobby projects, etc.
* emphasise that your results are the outcome of a collaboration with others
* extra-curricula activity that 

### Debatable hints


#### Don't say "I am smart, funny, and beautiful"

Whereas you are trying to sell yourself, you should avoid doing it in a "cheap way", basically writing something along
the lines of "You should hire me because I am smart, funny, and beautiful".

Don't write "I am a pro C++ developer". Write "I wrote a quantum simulation engine from scratch using C++ (20k lines of code)".

Don't write "I am an Agile project management expert". Write "I used Scrum-based Agile methodologies to organize my work during my PhD"

When you don't make a claim about your skills, but it rather emerges from your experience, it strikes.
For this reason it is recommended to avoid the section "Skills" with ambiguous self-assessed scores.
Rather, expand the "Work Experience" section and mention what prove the skills you want to the reader to notice.

!!! danger "Why is this debatable"

    Not everyone may agree with this. Here some reasons why:

    * For recruiters it is easier to scroll through a simple list, and they are more visible.
      *Counter argument: you can emphasize the skills with the bold style.*


#### Add a small introduction on the top

It is generally appreciated if you add few concise sentences at the top of your Resume which summarize your application.

Typically, you should write 1-2 sentences to introduce yourself and highlight your qualifications.
Following this, you could add 1 sentence describing what you are looking for (position, company, activities).
Keep the latter sentence neither too broad nor to narrow.

!!! danger "Why is this debatable"

    Such statements should anyway be part of the cover letter. *Counter argument: a recruiter may look only into the Resumes.*

#### Include that you have a PhD

In case you have achieved a PhD, you should definitely mention it and make it stand out, especially if you are at the 
beginning of your professional career.

!!! danger "Why is this debatable"

    Reportidely, some companies exclude candidates with a PhD because overskilled and ambitious. 
    For this reason, it is not uncomoon to hear people suggesting to omit it altogether.
    *Counter argument: are you sure you want to work for such a company?*

## Automatically scannable Resume

Often Recruiters use tools to automatically scan Resumes and sort them out before (or instead of) doing it manually. 
You can use the same tools on your own Resume before submitting it to see what would be the result.

A free online scanner can be found [here](https://www.resumego.net/resume-checker/). 
Just search for "scan resume free" on the internet to find other engines.

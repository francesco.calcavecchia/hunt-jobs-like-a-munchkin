# Resume examples

In the following you can read the stories of Tommaso and Jeanine, how was their Resume before and after.


## Jeanine

...


## Tommaso

![Placeholder](img/authors/tommaso-gorni.png){ align=left width="28" loading=lazy }

*Tommaso Gorni, 3 Jul 2022*

My starting point, and actually my only known paradigm, was a standard academical CV, a three-page document with the first one dedicated to my education and expertise, and the last two to my scientific publications and talks. 

[My-academic-CV](documents/CV-Tommaso.pdf)

### What I changed

During a discussion with other scientists who were trying to convert their CVs into some non-academical format as well, I got some very useful advice which I can sum up into:

 1. Shorten it.
    * Simplify the publication list (keep only title and link).
    * Remove the list of talks (condense it to number of participations to conferences as a speaker).
	* Condense the teaching section to one line.
 2. Make the CV more readable to non-specialists.
    * Try avoiding keywords specific only to a narrow academical field.
	* Add a short description of tasks and duties for each listed working experience. 


Moreover, I also included work experiences which were not relevant for academic positions (typesetter, in my case), and added two lines of introduction resuming my background.

[My-first-resume](documents/Resume-Tommaso.pdf)

### What can be improved

Along lines 1. and 2., some other improvements have come up after talking with Francesco, namely:

* Remove completely the publication list (keep only link to website like Google Scholar or attach as separate file).

* Merge Skills into Working experience: instead of list of tasks and duties, put short description of what you did using which tools. 

* Introductory lines are still too specific, make any person who didn't work in your field able to understand them.

[My-second-resume](Link-to-my-resume-2)
